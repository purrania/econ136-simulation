import hi
from hi import *
#my imports
import numpy as np
normal = np.random.normal
import random as rd
random = rd.random
import math 
from math import *
import parameters
from parameters import *
#code
registry = [[]]
patient_id = 0
cycle_number = 0
class donor:
    def __init__(self, patient_id, donor_id, kidney, patient_priority, patient_entry_cycle):
        self.patient_id = patient_id
        self.donor_id = donor_id
        self.kidney = kidney
        self.patient_priority = patient_priority
        self.patient_entry_cycle = patient_entry_cycle
class patient:
    def __init__(self, patient_id, kidney, priority, number_of_donors, entry_cycle, waiting_time):
        self.patient_id = patient_id
        self.kidney = kidney
        self.priority = priority
        self.number_of_donors = number_of_donors
        self.entry_cycle = entry_cycle
        self.waiting_time = waiting_time
class kidney: 
    def __init__(self, blood_type, HLA_type, cross_match_parameter):
        self.blood_type = blood_type
        self.HLA_type = HLA_type
        self.cross_match_parameter = cross_match_parameter
class pair:
    def __init__(self, patient, donors, desired):
        self.patient = patient
        self.donors = donors
        self.desired = desired
#If all else is equal - only take the top 10% of cross-match results
def cross_match(cross_match_parameter1, cross_match_parameter2):
    return abs(cross_match_parameter1 - cross_match_parameter2) < 0.75
def kidneys_match(kidney_1, kidney_2):
    return (
            kidney_1.blood_type == kidney_2.blood_type
        and (kidney_1.HLA_type == kidney_2.HLA_type)
        and  cross_match(kidney_1.cross_match_parameter, kidney_2.cross_match_parameter)
    )
#Debugging function
hello();
def dump_kidney(person,flag):
    kidney = person.kidney
    msg = '\t'
    if flag: msg += '\t'
    print(msg+'KIDNEY:', kidney.blood_type,kidney.HLA_type, kidney.cross_match_parameter)
def dump_patient(pair):
    patient = pair.patient
    print("PATIENT", patient.patient_id, "\nPRIORITY:", patient.priority, "\nENTRY CYCLE:", patient.entry_cycle)
    dump_kidney(patient,0)
def dump_donor(donor):
    print("\tDONOR", donor.donor_id)
    #print(donor.patient_id)
    dump_kidney(donor,1)
    #print(donor.patient_priority)
    #print(donor.patient_entry_cycle)

def dump_donors(somepair):
    donors = somepair.donors
    #print("HERE",len(donors), donors)
    if donors:
        for donor in donors:
            dump_donor(donor)
def dump_pair(somepair):
    dump_patient(somepair)
    dump_donors(somepair)
def dump_registry(registry):
    for priority in range(len(registry)):
        #print("PRIORITY: ", priority)
        priority_queue = registry[priority]
        for somepair in priority_queue:
            dump_pair(somepair)
# blood type proportions found at:
def initialise_blood_type():
    random_var = random()
    if random_var < 0.45:
        blood_type = "O"
    elif ((random_var - 0.45) < 0.4):
        blood_type = "A"
    elif ((random_var - 0.85) < 0.11):
        blood_type = "B"
    else:
        blood_type = "AB"
    return blood_type
def squash(x):
    return 5*x**(-2)
# HLA type proportions found at:
def initialise_HLA_type():
    random_var = ceil(squash(0.45)*(0.00000005+random()))
    return random_var
def initialise_cross_match_parameter():
    random_var = random()
    return random_var
def initialise_kidney():
    blood_type = initialise_blood_type()
    HLA_type = initialise_HLA_type()
    cross_match_parameter = initialise_cross_match_parameter()
    return kidney(blood_type, HLA_type, cross_match_parameter)
def initialise_waiting_time():
    return normal(mean_waiting_available, stdv_waiting_available)
def initialise_number_of_donors():
    return int(normal(mean_donors_per_patient, stdv_donors_per_patient))
def initialise_patient():
    new_patient = patient(patient_id, initialise_kidney(), 0, initialise_number_of_donors(), cycle_number, initialise_waiting_time())
    return new_patient
def initialise_donors(patient):
    donors = []
    for i in range(patient.number_of_donors):
        #print("II", i)
        new_donor = donor(patient_id, [patient_id, i], initialise_kidney(), 0, patient.entry_cycle)
        donors.append(new_donor)
    return donors
def initialise_pair():
    patient = initialise_patient()
    donors = initialise_donors(patient)
    desired = []
    new_pair = pair(patient,donors, desired) 
    new_pair.patient = patient
    new_pair.donors = donors
    new_pair.desired = desired
    return new_pair
def insertion_of_pairs(number_of_inserted_pairs, priority, registry):
    for i in range(number_of_inserted_pairs):
        new_pair = initialise_pair()
        #print("newpair")
        #print(i)
        registry[priority].append(new_pair)
        global patient_id
        patient_id += 1
def insert_new_pairs(registry):
    global pairs_inserted_this_cycle
    number_of_new_pairs = int(normal(mean_pairs_per_time_slice, stdv_pairs_per_time_slice))
    pairs_inserted_this_cycle = number_of_new_pairs
    insertion_of_pairs(number_of_new_pairs, 0, registry)
def match_and_update(registry):
    list_of_matches = find_matches_by_patient_id(registry)
    execute_matches(list_of_matches, registry)
def find_matches_by_patient_id(registry,):
    list_of_matches = []
    #print(len(registry),"YEEHAW")
    for priority in range(len(registry)):
        #print("HEEERREEEEDAAAA")
        priority_queue = registry[priority]
        for receiving_pair in priority_queue[::-1]:
            #print("HEREWWWWEEEE")
            candidate = seek_donor(receiving_pair, priority, registry)
            #print(candidate)
            if candidate[0]>=0 and candidate[1][1]>=0:
                #print("here")
                list_of_matches.append(candidate)
                #print(list_of_matches)
    global matches_this_cycle; matches_this_cycle = len(list_of_matches)
    return list_of_matches
    #print(list_of_matches)
def delete_patient(patient_id, registry):
    global cycle_number
    for i in range(len(registry)):
        for somepair in registry[i]:
            if somepair.patient.patient_id == patient_id:
                #print("KILLING PATIENT", pair.patient.patient_id," WITH PRIORITY ", pair.patient.priority)
                registry[i].remove(somepair)
def delete_donor( donor_id, registry):
    for i in range(len(registry)):
        priority_queue = registry[i]
        for somepair in priority_queue:
            donors = somepair.donors
            for donor in donors:
                #print(donor.donor_id, donor_id)
                if donor.donor_id == donor_id:
                    #print("KILLING DONOR", donor_id, " WITH PRIORITY ", pair.patient.priority)
                    donors.remove(donor)
                    #promote_pair(somepair.patient.patient_id, registry)
def promote_pair(patient_id, registry):
    global level_cap
    old_priority = level_cap-1
    #old_priority = len(registry)-1
    while old_priority >= 0:
        #print("OLDPR",old_priority)
        priority_queue = registry[old_priority]
        for somepair in priority_queue[::-1]:
            if somepair.patient.patient_id == patient_id:
                if old_priority < len(registry)-1:
                    registry[old_priority+1].append(somepair)
                    somepair.patient.priority += 1 
                    #print(somepair.patient.priority, old_priority+1)
                    assert(somepair.patient.priority == old_priority+1)
                else:
                    somepair.patient.priority += 1
                    registry.append([somepair])
                priority_queue.remove(somepair)
        old_priority-=1
def execute_matches(list_of_matches, registry):
    #print("exec", list_of_matches)
    if list_of_matches:
        for match in list_of_matches:
            patient_id = match[0] 
            donor_id = match[1]
            #print(patient_id, donor_id)
            delete_patient(patient_id, registry)
            delete_donor(donor_id, registry)
def get_patient_id(somepair):
    return somepair.patient.patient_id
def pair_matches(donating_pair, receiving_pair):
    match_info = [0, None, None]
    patient = receiving_pair.patient
    for donor in donating_pair.donors:
        if kidneys_match(donor.kidney, patient.kidney):
            match_info = [1, patient.patient_id, donor.donor_id]
            break
    return match_info
def seek_donor(receiving_pair, priority, registry):
    #print("HSSSSERE")
    global level_cap
    new_priority = level_cap - 1
    while new_priority>=0:
        #print("HEDDDDRE")
        priority_queue = registry[new_priority]
        for donating_pair in priority_queue:
            #print("HERE")
            match_info = pair_matches(donating_pair, receiving_pair)
            if match_info[0]:
                patient_id = match_info[1]
                donor_id = match_info[2]
                
                if patient_id not in already_paired and donor_id not in already_paired:
                    already_paired.append(patient_id)
                    already_paired.append(donor_id)
                    result = [patient_id, donor_id]
                    #print("YEEEHAHREHAHRA")
                    return result
        new_priority-=1
    return [-1,[-1,-1]]
def indicate_cycle_results():
    global cycle_number; global matches_this_cycle; global pairs_inserted_this_cycle
    print()
    print("Cycle Number:", cycle_number)    
    print("New pairs this cycle", pairs_inserted_this_cycle)
    print("Matches this Cycle:", matches_this_cycle)
def death_count():
    global registry
    global dead_people
    global cycle_number
    for priority_queue in registry:
        for pair in priority_queue:
            if pair.patient.entry_cycle < cycle_number - mean_waiting_available:
                dead_people+=1
                delete_patient(pair.patient.patient_id,registry)
def cross_score(kidney1, kidney2):
    return abs(kidney1.cross_match_parameter - kidney2.cross_match_parameter)
def find_desired(somepair, registry):
    TCC_next = None
    TCC_donor = None
    min_score = 1
    for priority in registry:
        for candidate in priority:
            for donor in candidate.donors:
                if kidneys_match(donor.kidney, somepair.patient.kidney):
                    new_score = cross_score(donor.kidney, somepair.patient.kidney)
                    if new_score < min_score:
                        min_score = new_score
                        TCC_next = candidate
                        TCC_donor = donor
    return [TCC_next,TCC_donor,min_score]
def try_cycle(somepair, registry, length):
    cycle = []
    while somepair.desired[0]!= None and somepair.desired[0] not in cycle:
        cycle.append(somepair)
        somepair = somepair.desired[0]
    if somepair.desired[0] in cycle:
        return cycle
free_list = []
already_paired = []
total_ever = 0
dead_people = 0
waiting_time_total = 0
old_total_in_registry = 0
multidonor = 1
while cycle_number < number_of_cycles_simulated:
    death_count() 
    matches_this_cycle = 0
    pairs_inserted_this_cycle = 0
    insert_new_pairs(registry)
    waiting_time_total += old_total_in_registry + pairs_inserted_this_cycle
    #print(registry[0]) 
    level_cap = len(registry)
    #match_and_update(registry)
    done = 0
    old_len = len(registry[0])
    new_len = -1
    #dump_registry(registry)
    while old_len != new_len:
        old_len = len(registry[0])
        # Recalculate preferences
        for priority in registry: 
            for somepair in priority:
                somepair.desired = find_desired(somepair, registry)
        # Consult the old donations
        for priority in registry:
            for somepair in priority:
                for somedonor in free_list:
                    if kidneys_match(somedonor.kidney, somepair.patient.kidney):
                        if cross_score(somedonor.kidney, somepair.patient.kidney) < somepair.desired[2]: 
                            free_list += somepair.donors
                            free_list.remove(somedonor)
                            delete_patient(somepair.patient.patient_id, registry)
        free_list = free_list[:2000]
        # Find cycles
        for priority in registry:
            for somepair in priority:
                cycle = try_cycle(somepair, registry, mean_donors_per_patient)
                if cycle!= None and len(cycle):
                    for otherpair in cycle:
                        #if multidonor:
                        free_list += otherpair.donors    
                        #print(type(otherpair))
                        delete_donor(otherpair.desired[1].donor_id, registry)
                        delete_patient(otherpair.patient.patient_id, registry)
                else:
                    done = 1 
                    break
        new_len = len(registry[0])
    #dump_registry(registry)
    indicate_cycle_results()
    #dump_registry(registry)
    #dump_matches()
    total_in_registry = 0
    cycle_number += 1
    for i in range(len(registry)):
        total_in_registry += len(registry[i])
        print("PRIORITY:",i,"PAIRS:",len(registry[i])) 
    #if len(registry) == 2:
        #for somepair in registry[1]:
            #dump_pair(somepair)
    total_ever += pairs_inserted_this_cycle
    percentage_ever_matched = (total_ever - total_in_registry - dead_people)/total_ever
    print("TOTAL IN REGISTRY", total_in_registry)
    print("PERCENTAGE OF ALL PATIENTS EVER MATCHED", percentage_ever_matched)
    print("DEAD PEOPLE", dead_people, "\nTOTAL EVER PEOPLE", total_ever)
    percentage_dead = (dead_people)/total_ever
    print("PERCENTAGE OF DEAD PATIENTS", percentage_dead)
    avg_time = waiting_time_total
    if percentage_ever_matched != 0:
        avg_time = waiting_time_total/(percentage_ever_matched*total_ever)
    print("AVERAGE WAITING TIME", avg_time)
    old_total_in_registry = total_in_registry
