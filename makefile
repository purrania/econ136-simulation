.SILENT:
all:
	echo Unpaired Multiple-Donor No Deceased List:	make ud-n
	echo Unpaired Multiple-Willing No Deceased List: make uw-n
	echo Unpaired Multiple-Donor, Yes Deceased List: make ud-y
	echo Unpaired Multiple-Willing Yes Deceased List: make uw-y
	echo Multiple-Donor Top Trading Cycles: make td
	echo Multiple-Willing Top Trading Cycles: make tw	
ud-n:
	python3 ud-n.py
uw-n:
	python3 uw-n.py
ud-y:
	python3 ud-y.py
uw-y:
	python3 uw-y.py
td:
	python3 td.py
tw:
	python3 tw.py
clean:
	rm -rf __pycache__
