# Macro parameters - feel free to play around with these :)
mean_pairs_per_time_slice = 100
stdv_pairs_per_time_slice = 0
mean_donors_per_patient = 1
stdv_donors_per_patient = 0
mean_waiting_available = 24
stdv_waiting_available = 0
number_of_cycles_simulated = 60
starting_priority = 0
